<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Tugas Laravel Templating
// Route::get('/', function () {
//     return view('tables.index');
// });

Route::get('/data-table', function () {
    return view('tables.data-table');
});

/*
Route::get('/master', function () {
    return view('layouts.master');
});
*/

// Tugas Laravel CRUD Kristian
// Route::get('/pertanyaan','PertanyaanController@index')->name('pertanyaan.index');
// Route::get('/pertanyaan/create','PertanyaanController@create');
// Route::post('/pertanyaan','PertanyaanController@store');
// Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');

// Route::resource('pertanyaanku','PertanyaanController');
Route::resource('pertanyaan','PertanyaanController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

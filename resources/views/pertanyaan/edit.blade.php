@extends('layouts.master')
@section('title','Edit Pertanyaan')
@section('content')
<div class="ml-3 mt-3">
<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="judul" id="title" value="{{$pertanyaan->judul}}" placeholder="Masukan Judul">
        @error('title')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Isi</label>
        <textarea type="text" class="form-control" name="isi" id="body" cols="30" rows="10" >{{$pertanyaan->isi}}</textarea>
        @error('body')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
</div>
@endsection
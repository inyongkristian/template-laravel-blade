@extends('layouts.master')
@section('title','Buat Pertanyaan')
@section('content')
<div class="ml-3 mt-3">
<form action="/pertanyaan" method="POST">
    @csrf
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="judul" id="title" placeholder="Masukan Judul">
        @error('title')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Isi</label>
        <textarea type="text" class="form-control" name="isi" id="body" cols="30" rows="10" placeholder="Masukan Isi"></textarea>
        @error('body')
            <div class="alert alert-danger">
                {{$message}}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
</div>
@endsection
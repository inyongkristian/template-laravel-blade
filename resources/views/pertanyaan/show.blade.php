@extends('layouts.master')
@section('title','Show Pertanyaan')
@section('content')
<div class="ml-3 mt-3">

    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
    <a href="/pertanyaan" class="btn btn-danger">Back</a>
</div
@endsection
@extends('layouts.master')
@section('title','List Pertanyaan')
@section('content')

<div class="ml-3 mt-3">
<a href="{{route('pertanyaan.create')}}" class="btn btn-primary mb-2">Tambah</a>
<table class="table">
    <thread class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            <th scope="col">Isi</th>
            <th scope="col">Actions</th>
        </tr>
    </thread>
    <tbody>
        @forelse ($pertanyaan as $key=>$value)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$value->judul}}</td>
            <td>{{$value->isi}}</td>
            <td style="display: flex;">
                <a href="{{route('pertanyaan.show', ['pertanyaan'=>$value->id])}}" class="btn btn-info">Show</a>
                <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/pertanyaan/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
        </tr>
        @empty
        <tr colspan="3">
            <td>No Data</td>
        </tr>
        @endforelse
    </tbody>
</table>
</div>
@endsection